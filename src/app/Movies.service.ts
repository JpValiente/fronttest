import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Movie from './Movie'

@Injectable({
    providedIn: 'root'
})

export class MovieService {
    url = 'http://localhost:8080/back-test/rest/movies';
    constructor(private http: HttpClient) {

    }

    public list(): Observable<Movie[]> {
        return this.http.get<Movie[]>(this.url);
      }

    public getById(id:number): Observable<Movie> {
        return this.http.get<Movie>(`${this.url}/${id}`)
    }

    public save(movie: Movie): Observable<any> {
        return this.http.post<Movie>(this.url,movie)
    }

    public update(id: number, movie: Movie): Observable<any> {
        return this.http.put<any>(`${this.url}/${id}`,movie)
    }

    public delete(id:number): Observable<any> {
        return this.http.get<any>(`${this.url}/${id}`)
    }

}
class Movie {

    idMovie: number
    title: String
    year: String
    duration: String

    random = (min:number, max:number) => Math.floor(Math.random() * (max - min)) + min;

    constructor(
        title: String,
        year: String,
        duration: String
    ) {
        this.idMovie = this.random(50000,1000000)
        this.title = title
        this.year = year
        this.duration = duration
    }


}

export default Movie
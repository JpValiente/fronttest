import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import Movie from './Movie';
import { MovieService } from './Movies.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front-test';
  movie:any = null;
  movieId:any = null
  allMovies:Array<Movie> = []
  movieTitle:String = ''
  duration:String = ''
  year:String = ''
  movieIdToDelete: any = null
  
  constructor(
    private movieService: MovieService,
    private httpClient: HttpClient
  ) {}

  getAllMovies() {
    this.movieService.list().subscribe((movies:Array<Movie>) => {
      this.allMovies = movies
    })
  }

  getSpecificMovie() {
    this.movieService.getById(this.movieId).subscribe((response) => {
      alert(response)
    })
  }

  createMovie() {
    let movie:Movie = new Movie(this.movieTitle,this.duration,this.year)
    this.movieService.save(movie).subscribe((response) => {
      alert(response)
    })
  }

  updateMovie() {
    let movie:Movie = new Movie(this.movieTitle,this.duration,this.year)
    this.movieService.update(this.movieId,movie).subscribe((response) => {
      alert(response)
    })
  }

  deleteMovie() {
    this.movieService.delete(this.movieIdToDelete).subscribe((response) => {
      alert('ok')
    })
  }
}
